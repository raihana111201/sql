jawaban nomor 1 :
create database myshop;

jawaban nomor 2 :
create table users(
	id int(11) primary key auto_increment,
	name varchar(255),
	email varchar(255),
	password varchar(255)
);

create table items(
	id int(11) primary key auto_increment,
	name varchar(255),
	description	varchar(255),
	price int,
	stock int,
	category_id int 
);

create table categories(
	id int(11) primary key auto_increment,
	name varchar(255)
);

ALTER TABLE items ADD FOREIGN KEY fk_items(category_id) REFERENCES categories(id);


jawaban nomor 3 :
insert into users values
("John Doe", "john@doe.com", "john123"),
("Jane Doe", "jane@doe.com", "jenita123");

insert into categories values
("gadget"),("cloth"),("men"),("women"),("branded");

insert into items values
("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1),
("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),
("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);


jawaban nomor 4 :
a : 
select id, name, email from users;
b :
select * from items where price>1000000;
select * from items where name like '%uniklo%';
c:
SELECT i.*, c.name FROM items i JOIN categories c ON i.category_id = c.id;


jawaban nomor 5 :
update items set price=2500000 where name="Sumsang b50";




